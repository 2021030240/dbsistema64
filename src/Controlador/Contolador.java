/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import modelo.*;
import Vista.jintProductos;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.JOptionPane;
import java.sql.SQLException;
import com.toedter.calendar.JDateChooser;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Administrator
 */
public class Contolador implements ActionListener {

    private jintProductos vista;
    private dbProductos db;
    boolean vr = false;

    public Contolador(jintProductos vista, dbProductos db) {
        this.vista = vista;
        this.db = db;
        this.deshabilitar();

        vista.btnBuscar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnDeshabilitar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.btnGuardar.addActionListener(this); // Añadir el listener para el botón "Guardar"
    }

    public void Limpiar() {
        vista.txtCodigo.setText("");
        vista.txtNombre.setText("");
        vista.txtPrecio.setText("");
        vista.JdateFecha.setDate(new Date());
    }

    public void cerrar() {
        int res = JOptionPane.showConfirmDialog(vista, "Desea cerrar el Sistema? ", "Productos", JOptionPane.YES_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (res == JOptionPane.YES_OPTION) {
            vista.dispose();
        }
    }

    public void habilitar() {
        vista.btnBuscar.setEnabled(true);
        vista.btnDeshabilitar.setEnabled(true);
        vista.txtCodigo.setEnabled(true);
        vista.txtNombre.setEnabled(true);
        vista.JdateFecha.setEnabled(true);
        vista.txtPrecio.setEnabled(true);
        vista.btnGuardar.setEnabled(true); // Habilitar el botón "Guardar"
    }

    public void deshabilitar() {
        vista.btnBuscar.setEnabled(false);
        vista.JdateFecha.setEnabled(false);
        vista.btnDeshabilitar.setEnabled(false);
        vista.txtCodigo.setEnabled(false);
        vista.txtNombre.setEnabled(false);
        vista.txtPrecio.setEnabled(false);
        vista.btnGuardar.setEnabled(false); // Deshabilitar el botón "Guardar"
        this.Limpiar();
    }

    public boolean validar() {
        boolean exito = true;
        if (vista.txtCodigo.getText().equals("") || vista.txtNombre.getText().equals("") || vista.txtPrecio.getText().equals("")) {
            exito = false;
        }
        return exito;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == vista.btnLimpiar) this.Limpiar();
        if (ae.getSource() == vista.btnCancelar) { this.Limpiar(); this.deshabilitar(); }
        if (ae.getSource() == vista.btnCerrar) this.cerrar();
        if (ae.getSource() == vista.btnNuevo) { this.habilitar(); this.vr = false; }
        if (ae.getSource() == vista.btnGuardar) {
            if (validar()) {
                Productos pro = new Productos();
                pro.setCodigo(vista.txtCodigo.getText());
                pro.setNombre(vista.txtNombre.getText());
                pro.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                pro.setFecha(convertirAMD(vista.JdateFecha.getDate()));

                try {
                    if (this.vr==false) {
                        db.insertar(pro);
                        this.ActualizarTabla(db.lista());
                        JOptionPane.showMessageDialog(vista, "Producto guardado exitosamente.");
                    } else {
                        db.actualizar(pro);
                        this.ActualizarTabla(db.lista());
                        JOptionPane.showMessageDialog(vista, "Producto actualizado exitosamente.");
                    }
                    this.Limpiar();
                    this.deshabilitar();
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(vista, "Error al guardar el producto: " + e.getMessage());
                }
            } else {
                JOptionPane.showMessageDialog(vista, "Por favor, complete todos los campos.");
            }
        }
        if (ae.getSource() == vista.btnBuscar) {
            Productos pro = new Productos();
            if (vista.txtCodigo.getText().equals("")) {
                JOptionPane.showMessageDialog(vista, "Falto capturar el codigo");
            } else {
                try {
                    pro = (Productos) db.buscar(vista.txtCodigo.getText());
                    if (pro.getIdProductos() != 0) {
                        vista.txtNombre.setText(pro.getNombre());
                        vista.txtPrecio.setText(String.valueOf(pro.getPrecio()));
                        convertirStringDate(pro.getFecha());
                        this.vr = true;
                        vista.btnDeshabilitar.setEnabled(true);
                        vista.btnGuardar.setEnabled(true);
                    } else {
                        JOptionPane.showMessageDialog(vista, "No se encontro");
                    }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(vista, "Surgio un eror" + e.getMessage());
                }
            }
        }
        if (ae.getSource() == vista.btnDeshabilitar) {
            int opcion = JOptionPane.showConfirmDialog(vista, "¿Deseas deshabilitar el producto", "Producto", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (opcion == JOptionPane.YES_OPTION) {
                Productos pro = new Productos();
                pro.setCodigo(vista.txtCodigo.getText());
                try {
                    db.deshabilitar(pro);
                    JOptionPane.showMessageDialog(vista, "El producto fue deshabilitado");
                    this.Limpiar();
                    this.deshabilitar();
                    this.ActualizarTabla(db.lista());
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(vista, "Surgio un error " + e.getMessage());
                }
            }
        }
    }

    public String convertirAMD(Date fecha) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(fecha);
    }

    public void convertirStringDate(String fecha) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-DD");
            Date date = dateFormat.parse(fecha);
            vista.JdateFecha.setDate(date);
        } catch (ParseException e) {
            System.err.print(e.getMessage());
        }
    }
    public void ActualizarTabla(ArrayList<Productos> arr){
        String campos[]={"idProducto","Codigo","Nombre","Precio","Fecha"};
        
        String[][] datos = new String[arr.size()][5];
        int renglon = 0;
        for(Productos registro : arr){
            datos[renglon][0] = String.valueOf(registro.getIdProductos());
            datos[renglon][1] = String.valueOf(registro.getCodigo());
            datos[renglon][2] = registro.getNombre();
            datos[renglon][3] = String.valueOf(registro.getPrecio());
            datos[renglon][4] = registro.getFecha();
            
           renglon++;
        
        }
        DefaultTableModel tb = new DefaultTableModel(datos,campos);
        vista.lista.setModel(tb);
    
    }

    public void iniciarVista(){
        vista.setTitle("Productos");
        vista.setSize(1200, 900); // Usar setSize en lugar de resize
        vista.setVisible(true);
        try{
           this.ActualizarTabla(db.lista());   
        }catch(Exception e){
            JOptionPane.showMessageDialog(vista, "Surgio Un error " + e.getMessage());
        }
    }
}